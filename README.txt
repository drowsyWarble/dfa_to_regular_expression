File Format
* Note - zero is not allowed in any of the file's inputs, it is reserved for the program's use

states : [1,2,3,4]
Note - states must start at 1 and ascend in increments of 1

accepting states
accepting sates : [1,3]

start state:
must always be 1

alphabet:
limited to two strings as long as they are not λ or θ

delta:
format for edge specification
[start edge],[end edge],[character/expression]

each edge should be separated by a whitespace and each value within the edge specification
should be delimited with a comma

Ex. DFA 2 from ass 15

1,2,3,4
2,3
1
a,b
1,2,a 1,3,b 2,3,b 2,2,a 3,2,a 3,4,b 4,4,a 4,4,b