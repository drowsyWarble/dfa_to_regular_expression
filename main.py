file = open("machine.txt").readlines()

filteredFile = []
for line in file:
    filteredFile.append(line.strip())

total_states = filteredFile[0].split(",")
accepting_states = filteredFile[1].split(",")
start_state = filteredFile[2]
input_language = filteredFile[3].split(",")
edge_lst = filteredFile[4].split(" ")

total_states.insert(0, '0')
total_states.append(len(total_states))

rmv_lst = []


def remove_sinks():
    # get rid of sinks
    for edge in range(len(edge_lst)):
        tmp_edge = edge_lst[edge].split(",")
        # if the edge is pointing to itself and has all of the alphabet in it
        if tmp_edge[0] == tmp_edge[1] and len(tmp_edge[2]) == len(input_language) + len(input_language) - 1:
            for line in range(len(edge_lst)):
                if tmp_edge[0] in edge_lst[line]:
                    rmv_lst.append(edge_lst[line])

    for i in rmv_lst:
        edge_lst.remove(i)


def combine_edges():
    edge_rmv_lst = []
    fused_edge_lst = []

    for edge_a in range(len(edge_lst)):
        for edge_b in range(edge_a, len(edge_lst)):
            outer_state_io = edge_lst[edge_a].split(",")[0] + edge_lst[edge_a].split(",")[1]
            inner_state_io = edge_lst[edge_b].split(",")[0] + edge_lst[edge_b].split(",")[1]

            # if they both have 2 digits that are the same
            if outer_state_io[0] == outer_state_io[1] and inner_state_io[0] == inner_state_io[1]:
                # prevents from adding the same position in array
                if edge_lst[edge_a] != edge_lst[edge_b]:

                    if inner_state_io == outer_state_io:
                        new_edge = edge_lst[edge_b][:3] + "," + edge_lst[edge_b][4:] + "+" + edge_lst[edge_a][4:]
                        edge_rmv_lst.append(edge_lst[edge_a])
                        edge_rmv_lst.append(edge_lst[edge_b])
                        if new_edge.split(",")[2][0] != new_edge.split(",")[2][2]:
                            fused_edge_lst.append(new_edge)

    for edge in set(edge_rmv_lst):
        if "+" not in edge:
            edge_lst.remove(edge)

    for edge in set(fused_edge_lst):
        edge_lst.append(edge)


def add_new_start_state():
    edge_lst.append("0,1,λ")


def modify_end_states():
    for state in accepting_states:
        edge_lst.append(state + ",f,λ")


def eliminate_state(in_index):
    incoming_edges = []
    outgoing_edges = []
    same_state_edge = []
    cur_state = total_states[in_index]
    new_edges = []

    # adds all incoming edges to a list
    for edge in edge_lst:
        cur_edge = edge.split(",")[1]
        if cur_edge == cur_state and cur_edge != edge.split(",")[0]:
            incoming_edges.append(edge)

    # adds all outging edges to a list
    for edge in edge_lst:
        cur_edge = edge.split(",")[0]
        if cur_edge == cur_state and cur_edge != edge.split(",")[1]:
            outgoing_edges.append(edge)

    # adds the case the case where a state points to itself into the list
    for edge in edge_lst:
        cur_edge = edge.split(",")[0]
        sec_edge = edge.split(",")[1]
        if cur_edge == sec_edge and cur_edge == cur_state:
            same_state_edge.append(edge)

    # main algorithm loop
    for edge_a in incoming_edges:
        for edge_b in outgoing_edges:
            # creates new edge
            cur_str = edge_a.split(",")[0] + "," + edge_b.split(",")[1] + ","
            # old ij
            for ij in edge_lst:
                if cur_str[:len(cur_str) - 1] in ij:
                    cur_str += "(" + ij.split(",")[2] + ")+"
            # old ik
            cur_str += edge_a.split(",")[2]

            # old kk*
            if len(same_state_edge) == 1:
                for i in same_state_edge:
                    cur_str += "(" + i.split(",")[2] + ")*"
            elif len(same_state_edge) == 0:
                cur_str += "θ*"
            # old kj
            cur_str += edge_b.split(",")[2]
            new_edges.append(cur_str)

    # cleanup methods
    for i in incoming_edges:
        edge_lst.remove(i)

    for i in outgoing_edges:
        edge_lst.remove(i)

    for i in same_state_edge:
        edge_lst.remove(i)

    # adds new edge to list
    for i in new_edges:
        edge_lst.append(i)


def final_combination():
    # assembles final regular expression
    fin_str = "0, f, "
    for i in range(len(edge_lst)):
        fin_str += edge_lst[i].split(",")[2]
        if i < len(edge_lst) -1:
            fin_str += "+"
    # clear the list for the final string
    del edge_lst[:]
    edge_lst.append(fin_str)


def main():
    add_new_start_state()
    modify_end_states()
    remove_sinks()
    print("original amount of states is", len(total_states) - 2)
    print("characters in alphabet are :", input_language)
    print("original dfa edges are : ", edge_lst)
    print()

    for i in range(1, len(total_states) - 1):
        combine_edges()
        eliminate_state(i)
        print("currently on pass", i, ":", edge_lst)

    print()
    final_combination()

    print("Final edge is from state",edge_lst[0][:1],"to", edge_lst[0][3:4], "with the regular expression")
    print(edge_lst[0][5:])


main()
